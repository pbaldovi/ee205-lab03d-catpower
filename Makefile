###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab03d - CatPower - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build CatPower program
###
### @author  Paulo Baldovi <pbaldovi@hawaii.edu>
### @date    02 Feb 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = g++
CFLAGS = -g -Wall

TARGET = catPower

all: $(TARGET)

catPower: catPower.cpp
	$(CC) $(CFLAGS) -o $(TARGET) catPower.cpp

clean:
	rm -f $(TARGET) *.o

